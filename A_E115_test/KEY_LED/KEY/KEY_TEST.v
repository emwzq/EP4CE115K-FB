
module KEY_TEST(
		KEY,
		led
		);					    // 模块名led
		
input	[2:0] 	KEY;		    //拨码开关
output	[2:0]	led;			//LED灯输出显示

assign led =	KEY;			//把拨码开关的数据在LED灯上面显示

endmodule