module  io_test
  (
    input k3,
    input k4,
    
    output  out1,
    output  out2
    
  );
  
  
  assign  out1 = k3;
  assign  out2  = k4;
  
  endmodule
  