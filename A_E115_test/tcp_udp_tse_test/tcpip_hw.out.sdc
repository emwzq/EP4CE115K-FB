## Generated SDC file "tcpip_hw.out.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Full Version"

## DATE    "Sat Feb 21 12:09:52 2015"

##
## DEVICE  "EP4CE40F23I7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {clk} -period 20.000  [get_ports {clk}]
create_clock -name {rx_clk} -period 8.000  [get_ports {rx_clk}]
create_clock -name {tx_clk} -period 40.000  [get_ports {tx_clk}]
#**************************************************************
# Create Generated Clock
#**************************************************************

derive_pll_clocks
set System_Clock       {pll_inst|altpll_component|auto_generated|pll1|clk[0]} 
set enet_pll_125       {pll_inst|altpll_component|auto_generated|pll1|clk[1]}

#**************************************************************
# Set Clock Latency
#**************************************************************

# Define the clocks that can appear on the output of the TX clock mux
# Create the 125MHz mux output
create_generated_clock -name tx_clk_125 -source [get_pins ${enet_pll_125}] -add [get_nets tse_tx_clk]
create_generated_clock -name enet_gtx_clk_125 -source [get_nets {tse_tx_clk}] -master_clock {tx_clk_125} -add [get_ports {gtx_clk}]

# Create the 25MHz mux output
create_generated_clock -name tx_clk_25  -source [get_ports {tx_clk}] -add [get_nets tse_tx_clk]
create_generated_clock -name enet_gtx_clk_25 -source [get_nets {tse_tx_clk}] -master_clock {tx_clk_25} -add [get_ports {gtx_clk}]

# This is a clock output, I/O constraints are not needed for output pin 
set_false_path -from * -to [get_ports gtx_clk]

# Cutting all the paths between clocks going into TSE, TSE provides clock crossing logic
set_clock_groups -exclusive \
    -group [get_clocks ${System_Clock} ] \
    -group [get_clocks {tx_clk_125 enet_gtx_clk_125} ] \
    -group [get_clocks {tx_clk_25  enet_gtx_clk_25 } ] \
	-group [get_clocks rx_clk ]
 
#**************************************************************
# Output Delay Constraints (Edge Aligned, Same Edge Capture)
#**************************************************************   
# max delay = tsu (-0.9) + skew (0.075) 
# min delay = -th ( 2.7) - skew (0.075)   
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_125] -max -0.825 [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_125] -min -2.775 [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_125] -max -clock_fall -0.825 [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_125] -min -clock_fall -2.775 [get_ports {tx_en tx_d*}]
    
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_25] -max -0.825  [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_25] -min -2.775 [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_25] -max -clock_fall -0.825  [get_ports {tx_en tx_d*}]
set_output_delay -add_delay -clock [get_clocks enet_gtx_clk_25] -min -clock_fall -2.775 [get_ports {tx_en tx_d*}]

# On same edge capture DDR interface the paths from RISE to FALL and
# from FALL to RISE on not valid for setup analysis
set_false_path -setup \
    -rise_from [get_clocks {tx_clk_125 tx_clk_25 }] \
    -fall_to [get_clocks {enet_gtx_clk_125 enet_gtx_clk_25}]

set_false_path -setup \
    -fall_from [get_clocks {tx_clk_125 tx_clk_25}] \
    -rise_to [get_clocks {enet_gtx_clk_125 enet_gtx_clk_25}]

# On same edge capture DDR interface the paths from RISE to RISE and
# FALL to FALL are not valid for hold analysis
set_false_path -hold \
    -rise_from [get_clocks {tx_clk_125 tx_clk_25}] \
    -rise_to [get_clocks {enet_gtx_clk_125 enet_gtx_clk_25}]

set_false_path -hold \
    -fall_from [get_clocks {tx_clk_125 tx_clk_25}] \
    -fall_to [get_clocks {enet_gtx_clk_125 enet_gtx_clk_25}]


#**************************************************************
# Input Delay Constraints (Center aligned, Same Edge Analysis)
#**************************************************************
# max delay = tco (-1.2) + skew (0.075)
# min delay = tcomin (-2.8) - skew (0.075)

# Constraint the path to the rising edge of the phy clock
#set_input_delay -add_delay -clock rx_clk -max -41.125 [get_ports {rx_d* }]
#set_input_delay -add_delay -clock rx_clk -min -42.875 [get_ports {rx_d*  }]

# Constraint the path to the falling edge of the phy clock
#set_input_delay -add_delay -clock rx_clk -max -clock_fall -41.125 [get_ports { rx_d*}]
#set_input_delay -add_delay -clock rx_clk -min -clock_fall -42.875 [get_ports { rx_d*}]


# On a same edge capture DDR interface the paths from RISE to FALL and
# from FALL to RISE on not valid for setup analysis
set_false_path -setup \
    -rise_from [get_clocks {rx_clk}] \
    -fall_to [get_clocks {rx_clk}]

set_false_path -setup \
    -fall_from [get_clocks {rx_clk}] \
    -rise_to [get_clocks {rx_clk}]

# On a same edge capture DDR interface the paths from RISE to RISE and
# FALL to FALL are not avlid for hold analysis
set_false_path -hold \
    -rise_from [get_clocks {rx_clk}] \
    -rise_to [get_clocks {rx_clk}]

set_false_path -hold \
    -fall_from [get_clocks {rx_clk}] \
    -fall_to [get_clocks {rx_clk}]   

#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************

#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

