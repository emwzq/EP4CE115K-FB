//////////////////////////////////////////////////////////////////////
////                                                              ////
////  eth_fsm                                                     ////
////                                                              ////
////  Description                                                 ////
////      network flow control                                    ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////

import type_defs::*;

module eth_fsm(
    input clk,
    input rst_n,
    input is_link_up,

    headers_if if_headers_rx,
    input frame_type_t rx_type,
    input rx_done,
    
    headers_if if_headers_tx,
    output frame_type_t tx_type,
    output logic tx_start,

    input [31:0] data_recv,
    input [15:0] data_recv_len,
    input data_recv_valid,
    input data_recv_start,
    output logic rx_done_clear,
    input u32_t cur_ripaddr,
    input u16_t cur_rport,

    input fifo_rdreq,
    output [31:0] fifo_q,
    output fifo_empty,
    input pkt_send_eop,

    output logic [13:0]  tx_dword_count
	 );
tcp_conn_t tcp_conn[0:3];

mac_header_t rx_mac_header;
arp_header_t rx_arp_header;
ipv4_header_t rx_ip_header;
icmp_headr_t rx_icmp_header;
udp_header_t rx_udp_header;
tcp_header_t rx_tcp_header;

mac_header_t tx_mac_header;  
arp_header_t tx_arp_header;  
ipv4_header_t tx_ip_header;
icmp_headr_t tx_icmp_header;
psdheader_t tx_psdheader;
udp_header_t tx_udp_header;
tcp_header_t tx_tcp_header;
tcp_opts_header_t tx_tcp_opts_header;



u16_t data_send_len;
wire fifo_wrreq;
wire [31:0] fifo_data_in;
tx_fifo tx_fifo_inst(
    .clock(clk),
    .aclr (pkt_send_eop),
    .data(fifo_data_in),
    .rdreq(fifo_rdreq),
    .wrreq(fifo_wrreq),
    .empty(fifo_empty),
    .full(),
    .q(fifo_q),
    .usedw());

assign fifo_data_in = data_recv;
assign fifo_wrreq = data_recv_valid;
assign data_send_len = data_recv_len;

//data checksum
logic [16:0] data_checksum_comb1;
logic [16:0] data_checksum_comb;
logic [31:0] data_checksum_in;
logic [15:0] data_checksum;
logic data_checksum_start;
logic data_valid;

assign data_checksum_comb1 = (data_checksum_start ? 16'b0 : data_checksum ) + data_checksum_in[31:16];
assign data_checksum_comb = data_checksum_comb1[15:0] + 
                            data_checksum_comb1[16] + data_checksum_in[15:0];

//data checksum inputs
assign data_checksum_in = fifo_data_in;
assign data_valid = fifo_wrreq;
assign data_checksum_start = data_recv_start;

always @(posedge clk or negedge rst_n)
if (!rst_n)begin
    data_checksum <= 16'b0;
end
else if (pkt_send_eop)begin
    data_checksum <= 0;
end
else if (data_valid) begin
    data_checksum <= data_checksum_comb[15:0] + data_checksum_comb[16];
end


//header checksum
logic [16:0] header_checksum_comb1;
logic [16:0] header_checksum_comb;
logic [31:0] header_checksum_in;
logic [15:0] header_checksum;
logic [13:0] hcs_dwords_count;
logic [3:0] hcs_dwords_count_init_val;
logic header_checksum_start;
logic [15:0] hcs_init_val;
logic header_checksum_done;
logic header_checkingsum;

assign header_checksum_comb1 = header_checksum + header_checksum_in[31:16];
assign header_checksum_comb = header_checksum_comb1[15:0] + 
                            header_checksum_comb1[16] + header_checksum_in[15:0];

//header checksum inputs
logic [0:4][31:0] header_buff;
logic [0:7][31:0] psdheader_buff;
logic psdheader_sel;

assign header_buff = if_headers_tx.data_in[223-:$bits(header_buff)];
assign psdheader_buff = {tx_psdheader,
    if_headers_tx.data_in[223-:$bits(psdheader_buff)-$bits(tx_psdheader)]};

assign header_checksum_in = psdheader_sel ? 
        psdheader_buff[hcs_dwords_count] : header_buff[hcs_dwords_count];


always @(posedge clk or negedge rst_n)
if (!rst_n) begin
    header_checksum <= 16'b0;
    hcs_dwords_count <= 14'b0;
    header_checksum_done <= 1'b0;
    header_checkingsum <= 1'b0;
end
else if(header_checksum_start)begin
    header_checksum <= hcs_init_val;
    hcs_dwords_count <= hcs_dwords_count_init_val - 1'b1;
    header_checksum_done <= 1'b0;
    header_checkingsum <= 1'b1;
end
else if (hcs_dwords_count >= 0 && header_checkingsum) begin 
    header_checksum <= header_checksum_comb[15:0] + header_checksum_comb[16];
    hcs_dwords_count <= hcs_dwords_count - 1'b1;
    if (hcs_dwords_count == 0) begin
        header_checksum_done <= 1'b1;
        header_checkingsum <= 1'b0;
    end
end
else if (header_checksum_done) begin 
    header_checksum_done <= 1'b0;
end

logic [1:0] cur_conn_idx;
always @(*) begin:find_cur_conn_idx
    automatic integer i;
    logic found;
    found = 1'b0;
    cur_conn_idx = 2'd0;
    for (i = 0; i < 4; i++) begin
        if (cur_ripaddr == tcp_conn[i].ripaddr && cur_rport == tcp_conn[i].rport) begin
            cur_conn_idx = 2'(i);
            found = 1'b1;
        end
    end
    if (found == 1'b0) begin
        for (i = 0; i < 4; i++) begin
            if (0 == tcp_conn[i].ripaddr && 0 == tcp_conn[i].rport) begin
                cur_conn_idx = 2'(i);
            end
        end
    end 
end

//eth state
typedef enum logic[3:0]{
    WaitForLinkUp = 4'b0,
    WaitFor2Sec,
    EthWorking,
    PrepareARPAddr,
    InsertArpTable,
    PrepareTCPAddr,
    TcpFSM
} EEthState;
logic [3:0] eth_state;

E_TCP_STATE next_tcp_state;

logic pkt_gen_start;
logic pkt_gen_busy;
logic [26:0] counter2sec;
logic tcp_reply1;

always @(posedge clk or negedge rst_n) 
if(~rst_n) begin
    eth_state <= WaitForLinkUp;
    tx_type <= T_ARP_REQUEST;
    pkt_gen_start <= 1'b0;
    counter2sec <= 0;
    rx_done_clear <= 1'b0;
end 
else begin
    rx_done_clear <= 1'b0;
    pkt_gen_start <= 1'b0;
    if (counter2sec > 0) begin
        counter2sec <= counter2sec - 1'b1;
    end
    case (eth_state)
    WaitForLinkUp: begin
        if (is_link_up) begin
            eth_state <=  WaitFor2Sec;
            counter2sec <= 27'h6000000;
        end
    end
    WaitFor2Sec : begin
        if (counter2sec == 0)
            eth_state <= EthWorking; 
    end
    EthWorking: begin/*
        if (counter2sec == 0 && !pkt_gen_busy &&
            dest_eth_addr == 48'haaaaaaaaaaaa) begin
                tx_type <= T_ARP_REQUEST;
                counter2sec <= 27'h6000000;
                pkt_gen_start <= 1'b1;
        end
        else*/
        if (rx_done && !pkt_gen_busy ) begin
            rx_done_clear <= 1'b1;
            if (rx_type == T_ICMP_REQUEST) begin
                tx_type <= T_ICMP_REPLY;
                pkt_gen_start <= 1'b1;
            end
            else if (rx_type == T_ARP_REPLY ||
                     rx_type == T_ARP_REQUEST) begin
                eth_state <= PrepareARPAddr;
            end
            else if (rx_type == T_UDP) begin
                tx_type <= T_UDP;
                pkt_gen_start <= 1'b1;
            end
            else if (rx_type == T_TCP) begin
                eth_state <= PrepareTCPAddr;
                tx_type <= T_TCP;
                
            end
        end
        else if (tcp_conn[cur_conn_idx].tcp_state == TCP_CLOSE_WAIT && !pkt_gen_busy) begin
            eth_state <= PrepareTCPAddr;
            tx_type <= T_TCP;
        end
    end
    PrepareARPAddr : begin
        eth_state <= InsertArpTable;
    end
    InsertArpTable : begin
        eth_state <= EthWorking;
        if (rx_type == T_ARP_REQUEST &&
            rx_arp_header.dst_ip_addr == IP_Addr) begin
                tx_type <= T_ARP_REPLY;
                pkt_gen_start <= 1'b1;
        end
    end
    PrepareTCPAddr : begin
        eth_state <= TcpFSM;
    end
    TcpFSM : begin
        eth_state <= EthWorking;
        if (next_tcp_state == TCP_SYN_RCVD && tcp_conn[cur_conn_idx].tcp_state != TCP_SYN_RCVD ||
            next_tcp_state == TCP_CLOSE_WAIT && tcp_conn[cur_conn_idx].tcp_state != TCP_CLOSE_WAIT ||
            next_tcp_state == TCP_LAST_ACK && tcp_conn[cur_conn_idx].tcp_state != TCP_LAST_ACK ||
            tcp_conn[cur_conn_idx].tcp_state == TCP_ESTABLISHED && tcp_reply1) begin
            pkt_gen_start <= 1'b1;
        end
    end
    endcase
end


//ARP Table
arp_entry_t arp_table[0:3];
logic [47:0] dest_eth_addr;
logic [1:0] arp_write_pointer; 
logic arp_table_wr;

assign arp_table_wr = eth_state == InsertArpTable &&
         rx_arp_header.dst_ip_addr == IP_Addr;

always @(*) begin:match_eth_addr
    automatic integer i;
    dest_eth_addr = 48'haaaaaaaaaaaa;
    for (i = 0; i < 4; i++) begin
        if (cur_ripaddr == arp_table[i].ipaddr)
            dest_eth_addr = arp_table[i].ethaddr;
    end
end



always @(posedge clk or negedge rst_n) 
if(~rst_n) begin:reset_arp_table
    automatic integer i;
    arp_write_pointer <= 0;
    for (i = 0; i < 4; i++) begin
        arp_table[i] <= '0;
    end
end 
else if (arp_table_wr) begin
    arp_write_pointer <= arp_write_pointer + 1'b1;
    arp_table[arp_write_pointer] <= {rx_arp_header.src_ip_addr, rx_arp_header.src_addr};
end

logic need_ack;

assign ack_correct = rx_tcp_header.ackno == tcp_conn[cur_conn_idx].snd_nxt;
assign need_ack = data_recv_len > 0;

always @(*) begin
    tcp_reply1 = 1'b0;
    next_tcp_state = tcp_conn[cur_conn_idx].tcp_state;
    if (rx_tcp_header.flags.RST)
        next_tcp_state = TCP_LISTEN;
    else
    case (tcp_conn[cur_conn_idx].tcp_state)
    TCP_LISTEN : if (rx_tcp_header.flags.SYN == 1'b1) begin //send SYN,ACK
        next_tcp_state = TCP_SYN_RCVD;
    end
    TCP_SYN_RCVD : if (rx_tcp_header.flags.ACK == 1'b1 && ack_correct) begin
        next_tcp_state = TCP_ESTABLISHED;
    end
    TCP_ESTABLISHED : begin
        if (rx_tcp_header.flags.FIN == 1'b1) begin//send ack
            next_tcp_state = TCP_CLOSE_WAIT;
        end
        else if (rx_tcp_header.flags.ACK && ack_correct && need_ack) begin
            tcp_reply1 = 1'b1;
        end
        else if (rx_tcp_header.flags.ACK) begin
            tcp_reply1 = 1'b0;
        end
    end
    TCP_CLOSE_WAIT : begin   //send fin, ack
        next_tcp_state = TCP_LAST_ACK;
    end
    TCP_LAST_ACK : if (rx_tcp_header.flags.ACK == 1'b1) begin
        next_tcp_state = TCP_LISTEN;
    end
    default : next_tcp_state = tcp_conn[cur_conn_idx].tcp_state;
    endcase
end


logic [31:0] seqno_init;
logic [17:0] fivems_timer;
logic fivems_timeout;

always @(posedge clk or negedge rst_n)
if (~rst_n) begin
    seqno_init <= 1;
    fivems_timer <= 0;
    fivems_timeout <= 0;
end
else begin
    fivems_timeout <= 1'b0;
    fivems_timer <= fivems_timer + 1'b1;
    if (fivems_timer == 18'h3d089) begin
        fivems_timer <= 18'h0;        
        fivems_timeout <= 1'b1;
        seqno_init <= seqno_init + 641;
    end
    else if (eth_state == TcpFSM && next_tcp_state == TCP_ESTABLISHED &&
            tcp_conn[cur_conn_idx].tcp_state != TCP_ESTABLISHED) begin
        seqno_init <= seqno_init + 64000;
    end
end

always@(posedge clk or negedge rst_n)
if (~rst_n) begin:reset_tcp_conn
    automatic integer i;
    for (i = 0; i < 4; i++) begin
        tcp_conn[i] <= '0;
        tcp_conn[i].tcp_state <= TCP_LISTEN;
    end
end
else if (eth_state == TcpFSM)begin
    tcp_conn[cur_conn_idx].tcp_state <= next_tcp_state;
    if (next_tcp_state == TCP_SYN_RCVD) begin
        tcp_conn[cur_conn_idx].ripaddr <= cur_ripaddr;
        tcp_conn[cur_conn_idx].rport <= rx_tcp_header.srcport;
        tcp_conn[cur_conn_idx].rcv_nxt <= rx_tcp_header.seqno;
        tcp_conn[cur_conn_idx].snd_nxt <= seqno_init;
        tcp_conn[cur_conn_idx].len <= 16'd0;
        tcp_conn[cur_conn_idx].mss <= if_headers_rx.tcp_opt_mss;
        tcp_conn[cur_conn_idx].timer <= 0;
        tcp_conn[cur_conn_idx].num_retrans <= 0;
    end
    else if (tcp_conn[cur_conn_idx].tcp_state == TCP_LAST_ACK && rx_tcp_header.flags.ACK == 1'b1)begin
        tcp_conn[cur_conn_idx].ripaddr <= 0;
        tcp_conn[cur_conn_idx].rport <= 0;
    end
end
else if (pkt_gen_state == TcpHeaderGen1 && header_checksum_done) begin
    tcp_conn[cur_conn_idx].snd_nxt <= tcp_conn[cur_conn_idx].snd_nxt + tx_tcp_header.flags.SYN + tx_tcp_header.flags.FIN + data_send_len;
    tcp_conn[cur_conn_idx].len <= data_send_len;
end
else if (pkt_gen_state == TcpHeaderGen0) begin
    tcp_conn[cur_conn_idx].rcv_nxt <= tcp_conn[cur_conn_idx].rcv_nxt + rx_tcp_header.flags.SYN + rx_tcp_header.flags.FIN + data_recv_len;
end


typedef enum logic [3:0] {
    Idle = 4'b1,
    MacHeaderGen,
    ArpHeaderGen,
    IpHeaderGen0,
    IpHeaderGen1,
    IcmpHeaderGen0,
    IcmpHeaderGen1,
    UdpHeaderGen0,
    UdpHeaderGen1,
    TcpHeaderGen0,
    TcpHeaderGen1,
    TcpOptionsGen,
    SendDataStart
}Epkt_gen_state;

logic [3:0] pkt_gen_state, next_pkt_gen_state;

logic [15:0] ipid;
always @(*) begin
    next_pkt_gen_state = pkt_gen_state;
    case (pkt_gen_state)
    Idle: begin
        if (pkt_gen_start) begin
            next_pkt_gen_state = MacHeaderGen;
        end
    end
    MacHeaderGen: begin
        if (tx_type == T_ARP_REQUEST) begin
            next_pkt_gen_state = ArpHeaderGen;
        end
        else if (tx_type == T_ARP_REPLY) begin
            next_pkt_gen_state = ArpHeaderGen;
        end
        else if (dest_eth_addr == 48'haaaaaaaaaaaa) begin
            next_pkt_gen_state = Idle;
        end
        else begin
            next_pkt_gen_state = IpHeaderGen0;
        end
    end
    ArpHeaderGen:begin
        next_pkt_gen_state = SendDataStart;
    end
    IpHeaderGen0:begin
        next_pkt_gen_state = IpHeaderGen1;
    end
    IpHeaderGen1:begin
        if (header_checksum_done) begin
            if (tx_type == T_ICMP_REPLY || tx_type == T_ICMP_REQUEST) begin
                next_pkt_gen_state = IcmpHeaderGen0;
            end
            else if (tx_type == T_UDP) begin
                next_pkt_gen_state = UdpHeaderGen0;
            end
            else if (tx_type == T_TCP) begin
                next_pkt_gen_state = TcpHeaderGen0;
            end
        end
    end
    IcmpHeaderGen0:begin
        next_pkt_gen_state = IcmpHeaderGen1;
    end
    IcmpHeaderGen1:begin
        if (header_checksum_done) begin
            next_pkt_gen_state = SendDataStart;
        end
    end
    UdpHeaderGen0:begin
        next_pkt_gen_state = UdpHeaderGen1;
    end
    UdpHeaderGen1:begin
        if (header_checksum_done) begin
            next_pkt_gen_state = SendDataStart;
        end
    end
    TcpHeaderGen0 : begin
        next_pkt_gen_state = TcpHeaderGen1;
    end
    TcpHeaderGen1 : begin
        if (header_checksum_done) begin
            if (if_headers_tx.is_option_exist)
                next_pkt_gen_state = TcpOptionsGen;
            else
                next_pkt_gen_state = SendDataStart;
        end
    end
    TcpOptionsGen : begin
        next_pkt_gen_state = SendDataStart;
    end
    SendDataStart : begin
        if (pkt_send_eop)
            next_pkt_gen_state = Idle;
    end
    default: begin
        next_pkt_gen_state = 'x;
    end
    endcase
end

always @(posedge clk or negedge rst_n)
if (~rst_n) begin
    pkt_gen_state <= Idle;
end
else begin
    pkt_gen_state <= next_pkt_gen_state;
end

always @(posedge clk or negedge rst_n)
if (!rst_n) begin
    ipid <= 16'b0;
    header_checksum_start <= 1'b0;
    hcs_dwords_count_init_val <= 4'b0;
    tx_dword_count <= 14'd0;
    psdheader_sel <= 1'b0;
end
else begin
    case (pkt_gen_state)
    Idle: begin
        if (next_pkt_gen_state == MacHeaderGen) begin
            ipid <= ipid + 1'b1;
            psdheader_sel <= 1'b0;
        end
    end
    IpHeaderGen0:begin
        header_checksum_start <= 1'b1;
        hcs_dwords_count_init_val <= 4'd5;
    end
    IpHeaderGen1:begin
        header_checksum_start <= 1'b0;
    end
    IcmpHeaderGen0:begin
        header_checksum_start <= 1'b1;
        hcs_dwords_count_init_val <= 4'd2;
    end
    IcmpHeaderGen1:begin
        header_checksum_start <= 1'b0;
        tx_dword_count <= 14'((data_send_len+3)/4 + 2 + 5 + 4);
    end
    UdpHeaderGen0:begin
        header_checksum_start <= 1'b1;
        psdheader_sel <= 1'b1;
        hcs_dwords_count_init_val <= 4'd5;
    end
    UdpHeaderGen1:begin
        header_checksum_start <= 1'b0;
        tx_dword_count <= 14'((data_send_len+2+3)/4 + 2 + 5 + 4);
    end
    TcpHeaderGen0:begin
        header_checksum_start <= 1'b1;
        psdheader_sel <= 1'b1;
        hcs_dwords_count_init_val <= 4'd8;
    end
    TcpHeaderGen1:begin
        header_checksum_start <= 1'b0;
        tx_dword_count <= 14'((data_send_len+2+3)/4 + 
            tx_tcp_header.header_len +  5 + 4);
    end
    endcase
end
assign tx_start = pkt_gen_state == SendDataStart;


logic [16:0] data_add_options_checksum;
assign data_add_options_checksum = data_checksum + 16'h2707;
always @* begin
    if (pkt_gen_state == IpHeaderGen1)
        hcs_init_val = 0;
    else if (pkt_gen_state == TcpHeaderGen1 && if_headers_tx.is_option_exist)
        hcs_init_val = data_add_options_checksum[15:0] + data_add_options_checksum[16];
    else
        hcs_init_val = data_checksum;
end



assign pkt_gen_busy = pkt_gen_state != Idle;

always@(*) begin
    tx_mac_header = '0;
    tx_arp_header = '0;
    tx_ip_header = '0;
    tx_icmp_header = '0;
    tx_psdheader = '0;
    tx_udp_header = '0;
    tx_tcp_header = '0;
    tx_tcp_opts_header = '0;
    case (pkt_gen_state)
    MacHeaderGen : begin
        tx_mac_header.src_addr = MAC_Addr;
        tx_mac_header.dst_addr = '0;
        tx_mac_header.mac_type = '0;
        if (tx_type == T_ARP_REQUEST) begin
            tx_mac_header.dst_addr = '1;
            tx_mac_header.mac_type = 16'h806;
        end
        else if (tx_type == T_ARP_REPLY) begin
            tx_mac_header.dst_addr = rx_mac_header.src_addr;
            tx_mac_header.mac_type = 16'h806;
        end
        else begin
            tx_mac_header.dst_addr = dest_eth_addr;
            tx_mac_header.mac_type = 16'h800;    
        end
    end
    ArpHeaderGen:begin
        tx_arp_header.hardware_type = 16'd1;
        tx_arp_header.protocol_type = 16'h0800;
        tx_arp_header.hardware_addr_len = 8'd6;
        tx_arp_header.protocol_addr_len = 8'd4;
        tx_arp_header.src_addr = MAC_Addr;
        tx_arp_header.src_ip_addr = IP_Addr;
        tx_arp_header.operation_type = '0;
        tx_arp_header.dst_addr = '0;
        tx_arp_header.dst_ip_addr = '0;
       if (tx_type == T_ARP_REQUEST) begin
            tx_arp_header.operation_type = ARP_REQUEST;
            tx_arp_header.dst_addr = '0;
            tx_arp_header.dst_ip_addr = TargetIP_Addr;
        end
        else if (tx_type == T_ARP_REPLY) begin
            tx_arp_header.operation_type = ARP_REPLY;
            tx_arp_header.dst_addr = rx_arp_header.src_addr;
            tx_arp_header.dst_ip_addr = rx_arp_header.src_ip_addr;
        end
    end
    IpHeaderGen0,IpHeaderGen1:begin
        tx_ip_header.version = 4'd4;
        tx_ip_header.header_len = 4'd5;
        if (tx_type == T_ICMP_REPLY)
            tx_ip_header.tos = 8'he0;
        else
            tx_ip_header.tos = 8'h00;
        if (tx_type == T_UDP || tx_type == T_ICMP_REPLY || tx_type == T_ICMP_REQUEST)    
            tx_ip_header.len = data_send_len < 18 ? 16'd46 : data_send_len + 16'd28;
        else if (tx_type == T_TCP)
            tx_ip_header.len = data_send_len + (if_headers_tx.is_option_exist ? 16'd48 : 16'd40);
		else 
			assert(0);
        tx_ip_header.ipid = ipid;
        tx_ip_header.b_no_fragment = 1'b1;
        tx_ip_header.ipoffset = 8'd0;
        tx_ip_header.ttl = 8'h80;
        case (tx_type)
        T_ICMP_REQUEST,T_ICMP_REPLY : tx_ip_header.proto = PROTO_ICMP;
        T_UDP : tx_ip_header.proto = PROTO_UDP;
        T_TCP : tx_ip_header.proto = PROTO_TCP;
        default : tx_ip_header.proto = 0;
        endcase
        tx_ip_header.src_ipaddr = IP_Addr;
        tx_ip_header.dst_ipaddr = rx_ip_header.src_ipaddr;
        if (header_checksum_done) begin
            tx_ip_header.checksum = ~header_checksum;
        end 
        else begin
           tx_ip_header.checksum = 16'b0;
       end
    end
    UdpHeaderGen0,UdpHeaderGen1:begin
        tx_psdheader.src_addr = IP_Addr;
        tx_psdheader.dst_addr = cur_ripaddr;
        tx_psdheader.mbz = 8'd0;
        tx_psdheader.protocal = PROTO_UDP;
        tx_psdheader.len = 16'(data_send_len + 8);

        tx_udp_header.srcport = UDP_Port;
        tx_udp_header.destport = rx_udp_header.srcport;
        tx_udp_header.len = tx_psdheader.len;
        if (header_checksum_done) begin
            tx_udp_header.checksum = ~header_checksum;
        end
        else begin
            tx_udp_header.checksum = 16'd0;
        end
    end
    TcpHeaderGen0, TcpHeaderGen1: begin
        tx_tcp_header.srcport = TCP_Port;
        tx_tcp_header.destport = tcp_conn[cur_conn_idx].rport;
        tx_tcp_header.seqno = tcp_conn[cur_conn_idx].snd_nxt;
        tx_tcp_header.ackno = tcp_conn[cur_conn_idx].rcv_nxt;
        tx_tcp_header.header_len = tcp_conn[cur_conn_idx].tcp_state == TCP_SYN_RCVD ? 4'd7 : 4'd5;
        tx_tcp_header.reserved = 4'b0;
        tx_tcp_header.flags.ECE = 1'b0;
        tx_tcp_header.flags.CWR = 1'b0;
        tx_tcp_header.flags.URG = 1'b0;
        tx_tcp_header.flags.ACK = 1'b0;
        tx_tcp_header.flags.PSH = 1'b0;
        tx_tcp_header.flags.RST = 1'b0;
        tx_tcp_header.flags.SYN = 1'b0;
        tx_tcp_header.flags.FIN = 1'b0;
        if (tcp_conn[cur_conn_idx].tcp_state == TCP_SYN_RCVD) begin
            tx_tcp_header.flags.ACK = 1'b1;
            tx_tcp_header.flags.SYN = 1'b1;
        end
        else if (tcp_conn[cur_conn_idx].tcp_state == TCP_CLOSE_WAIT) begin
            tx_tcp_header.flags.ACK = 1'b1;
        end
        else if (tcp_conn[cur_conn_idx].tcp_state == TCP_LAST_ACK) begin
            tx_tcp_header.flags.FIN = 1'b1;
            tx_tcp_header.flags.ACK = 1'b1;
        end
        else if (tcp_conn[cur_conn_idx].tcp_state == TCP_ESTABLISHED) begin
            tx_tcp_header.flags.ACK = 1'b1;
            tx_tcp_header.flags.PSH = 1'b1;
        end
        tx_tcp_header.window_size = 4096;
        tx_tcp_header.urgp = '0;

        if (header_checksum_done) begin
            tx_tcp_header.checksum = ~header_checksum;
        end 
        else begin
           tx_tcp_header.checksum = 16'b0;
        end

        tx_psdheader.src_addr = IP_Addr;
        tx_psdheader.dst_addr = cur_ripaddr;
        tx_psdheader.mbz = 8'd0;
        tx_psdheader.protocal = PROTO_TCP;
        tx_psdheader.len = 16'(tx_tcp_header.header_len * 4) + data_send_len;
    end
    TcpOptionsGen: begin
        tx_tcp_opts_header.kind_mss = 8'h2;
        tx_tcp_opts_header.mass_len = 8'h4;
        tx_tcp_opts_header.mss = 16'h1f00;
        tx_tcp_opts_header.kind_window_scale = 8'h3;
        tx_tcp_opts_header.window_scale_len = 8'h3;
        tx_tcp_opts_header.window_scale = 8'h3;
        tx_tcp_opts_header.end_of_opt = 8'h0;
    end
    IcmpHeaderGen0,IcmpHeaderGen1:begin
        tx_icmp_header.icmp_type = 8'b0;
        tx_icmp_header.code = 8'b0;
        tx_icmp_header.id = rx_icmp_header.id;
        tx_icmp_header.seqno = rx_icmp_header.seqno;
        if (header_checksum_done) begin
            tx_icmp_header.checksum = ~header_checksum;
        end
        else begin
            tx_icmp_header.checksum = 16'b0;
        end
    end
    default : begin
        tx_mac_header = '0;
        tx_arp_header = '0;
        tx_ip_header = '0;
        tx_icmp_header = '0;
    end
    endcase
end

always@(*) begin
    rx_mac_header = '0;
    rx_arp_header = '0;
    rx_ip_header = '0;
    rx_icmp_header = '0;
    rx_udp_header = '0;
    rx_tcp_header = '0;
    if_headers_tx.addr1 = '0;
    if_headers_tx.data_in = '0;
    if_headers_tx.wren = '0;
    if (eth_state == InsertArpTable) begin
        rx_arp_header = if_headers_rx.data_out[223:0];
    end
    else if (eth_state == TcpFSM) begin
        rx_tcp_header = if_headers_rx.data_out[159:0];
    end
    else begin//if (pkt_gen_busy) begin 
        case (pkt_gen_state) 
        MacHeaderGen : begin
            rx_mac_header = if_headers_rx.data_out[127:0];
            if_headers_tx.addr1 = RAM_MAC_ADDR;
            if_headers_tx.data_in = {tx_mac_header,{(224-$bits(tx_mac_header)){1'b0}}};
            if_headers_tx.wren = 1'b1;
        end
        ArpHeaderGen : begin
            rx_arp_header = if_headers_rx.data_out[223:0];
            if_headers_tx.addr1 = RAM_ARP_ADDR;
            if_headers_tx.data_in = tx_arp_header;
            if_headers_tx.wren = 1'b1;
        end
        IpHeaderGen0, IpHeaderGen1: begin
            rx_ip_header = if_headers_rx.data_out[159:0];
            if_headers_tx.addr1 = RAM_IP_ADDR;
            if_headers_tx.data_in = {tx_ip_header,{(224-$bits(tx_ip_header)){1'b0}}};
            if (header_checksum_done) begin
                if_headers_tx.wren = 1'b1;
            end
            else begin
                if_headers_tx.wren = 1'b0;
            end
        end
        IcmpHeaderGen0,IcmpHeaderGen1 : begin
            rx_icmp_header = if_headers_rx.data_out[63:0];
            if_headers_tx.addr1 = RAM_ICMP_ADDR;
            if_headers_tx.data_in = {tx_icmp_header,{(224-$bits(tx_icmp_header)){1'b0}}};
            if (header_checksum_done) begin
                if_headers_tx.wren = 1'b1;
            end
            else begin
                if_headers_tx.wren = 1'b0;
            end
        end
        UdpHeaderGen0,UdpHeaderGen1 : begin
            rx_udp_header = if_headers_rx.data_out[63:0];
            if_headers_tx.addr1 = RAM_UDP_ADDR;
            if_headers_tx.data_in = {tx_udp_header,{(224-$bits(tx_udp_header)){1'b0}}};
            if (header_checksum_done) begin
                if_headers_tx.wren = 1'b1;
            end
            else begin
                if_headers_tx.wren = 1'b0;
            end
        end
        TcpHeaderGen0, TcpHeaderGen1 : begin
            rx_tcp_header = if_headers_rx.data_out[159:0];
            if_headers_tx.addr1 = RAM_TCP_ADDR;
            if_headers_tx.data_in = {tx_tcp_header,{(224-$bits(tx_tcp_header)){1'b0}}};
            if (header_checksum_done) begin
                if_headers_tx.wren = 1'b1;
            end
            else begin
                if_headers_tx.wren = 1'b0;
            end
        end
        TcpOptionsGen : begin
            if_headers_tx.addr1 = RAM_TCP_OPTIONS_ADDR;
            if_headers_tx.data_in = {tx_tcp_opts_header,{(224-$bits(tx_tcp_opts_header)){1'b0}}};
            if_headers_tx.wren = 1'b1;
        end
        default : begin
            rx_mac_header = '0;
            rx_arp_header = '0;
            rx_ip_header = '0;
            rx_udp_header = '0;
            rx_tcp_header = '0;
            rx_icmp_header = '0;
            if_headers_tx.addr1 = '0;
            if_headers_tx.data_in = '0;
            if_headers_tx.wren = 1'b0;
        end
        endcase
    end
end

always@(*) begin
    if_headers_rx.addr2 = '0;
    if (eth_state == PrepareARPAddr) begin
        if_headers_rx.addr2 = RAM_ARP_ADDR;
    end
    else if (eth_state == PrepareTCPAddr) begin
        if_headers_rx.addr2 = RAM_TCP_ADDR;
    end
    else begin
        case (next_pkt_gen_state)
        MacHeaderGen : begin
            if_headers_rx.addr2 = RAM_MAC_ADDR;
        end
        ArpHeaderGen : begin
            if_headers_rx.addr2 = RAM_ARP_ADDR;
        end
        IpHeaderGen0, IpHeaderGen1 : begin
            if_headers_rx.addr2 = RAM_IP_ADDR;
        end
        IcmpHeaderGen0,IcmpHeaderGen1 : begin
            if_headers_rx.addr2 = RAM_ICMP_ADDR;
        end
        UdpHeaderGen0,UdpHeaderGen1 : begin
            if_headers_rx.addr2 = RAM_UDP_ADDR;
        end
        TcpHeaderGen0,TcpHeaderGen1 : begin
            if_headers_rx.addr2 = RAM_TCP_ADDR;
        end
        default : begin
            if_headers_rx.addr2 = RAM_IP_ADDR;
        end
        endcase
    end
end
assign if_headers_tx.be = '1;
assign if_headers_tx.sel = pkt_gen_state != Idle && pkt_gen_state != SendDataStart;    
assign if_headers_tx.is_option_exist = tcp_conn[cur_conn_idx].tcp_state == TCP_SYN_RCVD;

endmodule
