//////////////////////////////////////////////////////////////////////
////                                                              ////
////  mac_config                                                  ////
////                                                              ////
////  Description                                                 ////
////      config 88e1111 and tse module                           ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////
                                                             
import type_defs::*;

module mac_config(
	input clk,
	input rst_n,
    output reg [7:0] tse_addr,
    output reg tse_wr,
    output reg tse_rd,
    input  tse_busy,
    output reg [31:0] tse_wr_data,
    input  [31:0] tse_rd_data,
    output reg is_link_up,
    output reg is_1000m,
    output reg is_100m,
    output reg is_10m
);

reg [4:0] LUT_index;
reg [41:0] phy_config_data;
parameter 
	TotalConfigsNum = 24;
always@(*)
	case(LUT_index)
		5'h0:phy_config_data <= {1'b1,8'h0f,32'h0010};
		5'h1:phy_config_data <= {1'b1,8'h96,32'h800f};
		5'h2:phy_config_data <= {1'b1,8'h9b,32'h001f};
		5'h3:phy_config_data <= {1'b1,8'h84,32'h0de1};
		5'h4:phy_config_data <= {1'b1,8'h80,32'h1140};
		5'h5:phy_config_data <= {1'b1,8'h80,32'h9140};
		5'h6:phy_config_data <= {1'b0,8'h81,32'hffff};
        5'h7:phy_config_data <= {1'b0,8'h91,32'hffff};
        5'h8:phy_config_data <= {1'b1,8'h01,32'haaaaaaaa};        
        5'h9:phy_config_data <= {1'b1,8'h02,32'h04088033};        
        5'ha:phy_config_data <= {1'b1,8'h03,{MAC_Addr[23:16],MAC_Addr[31:24],MAC_Addr[39:32],MAC_Addr[47:40]}};
        5'hb:phy_config_data <= {1'b1,8'h04,{16'b0,MAC_Addr[7:0],MAC_Addr[15:8]}};
        5'hc:phy_config_data <= {1'b1,8'h17,32'h000c};
        5'hd:phy_config_data <= {1'b1,8'h05,32'h05ee};
        5'he:phy_config_data <= {1'b1,8'h06,32'h000f};
        5'hf:phy_config_data <= {1'b1,8'h07,32'h0000};
        5'h10:phy_config_data <= {1'b1,8'h08,32'h0010};
        5'h11:phy_config_data <= {1'b1,8'h09,32'h0010};
        5'h12:phy_config_data <= {1'b1,8'h0a,32'h0010};
        5'h13:phy_config_data <= {1'b1,8'h0b,32'h0008};
        5'h14:phy_config_data <= {1'b1,8'h0c,32'h0008};
        5'h15:phy_config_data <= {1'b1,8'h0d,32'h0008};
        5'h16:phy_config_data <= {1'b1,8'h0e,32'h000a};
        5'h17:phy_config_data <= {1'b1,8'h7a,32'h0001};
        5'h18:phy_config_data <= {1'b1,8'h02,32'h00800220};        
        5'h19:phy_config_data <= {1'b1,8'h02,32'h00802220};        
        5'h1a:phy_config_data <= {1'b1,8'h02,32'h00800223};
		default:phy_config_data <= {25'd0};
	endcase

reg [15:0] waiter;
reg [3:0] mac_init_state;
parameter
	Idle = 0,
	Wr = 1,
	Rd = 2,
	Fetch = 3,
	WaitForLinkUp = 5,
	SetMac0 = 6,
	SetMac1 = 7,
	SetConfig = 8,
	Done = 9;
	
always @(posedge clk or negedge rst_n)
if (!rst_n) begin
	mac_init_state <= Idle;
	tse_addr <= '0;
	tse_wr <= 1'b0;
	tse_rd <= 1'b0;
	tse_wr_data <= 0;
	LUT_index <= 0;
	waiter <= 0;
	is_link_up <= 0;
    is_1000m <= 1'b0;
    is_100m <= 1'b0;
    is_10m <= 1'b0;
end
else begin
	case (mac_init_state)
		Idle:begin
			if( waiter == 16'd2000 ) begin
				mac_init_state <= Fetch;
			end
			else begin
				waiter <= waiter + 16'd1;
			end
		end
		Wr:begin
			if (!tse_busy)begin
				mac_init_state <= Fetch;
				tse_wr <= 0;
			end
		end
		Rd: begin
        if (!tse_busy)begin
        	tse_rd <= 0;
        	if (tse_addr == 8'h81 && ((tse_rd_data & 4) == 0 || (tse_rd_data & 32) == 0))begin
        		mac_init_state <= WaitForLinkUp;
        		waiter <= 60000;
        	end
            else if (tse_addr == 8'h91 )begin
                is_1000m <= 1'b0;
                is_100m <= 1'b0;
                is_10m <= 1'b0;
                if(tse_rd_data[15:14] == 2'b10)begin
                    is_1000m <= 1'b1;
                end
                else if (tse_rd_data[15:14] == 2'b01)begin
                    is_100m <= 1'b1;
                end
                else if (tse_rd_data[15:14] == 2'b00)begin
                    is_10m <= 1'b1; 
                end
                mac_init_state <= Fetch;
            end
        	else begin
        		mac_init_state <= Fetch;
        	end
        end
		end
		WaitForLinkUp:begin
			if (waiter > 0)begin
				waiter <= waiter - 1'b1;
			end
			else begin
				mac_init_state <= Rd;
				tse_rd <= 1'b1;
				tse_addr <= 8'h81;
			end
		end
		Fetch: begin
			if (LUT_index < TotalConfigsNum)begin
				LUT_index <= LUT_index + 1'b1;
				if (phy_config_data[40])begin
					mac_init_state <= Wr;
					tse_wr <= 1'b1;
					tse_addr <= phy_config_data[39:32];
					tse_wr_data <= phy_config_data[31:0];
				end
				else begin
					mac_init_state <= Rd;
					tse_rd <= 1'b1;
					tse_addr <= phy_config_data[39:32];
				end
			end
			else begin
				tse_rd <= 1'b0;
				tse_wr <= 1'b0;
                is_link_up <= 1;
                mac_init_state <= Done;
			end
		end
		Done: begin
		end

	endcase
end


endmodule

