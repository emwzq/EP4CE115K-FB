//--------------------------------------------------------------------------------------------------
// Design    : tcpip_hw
// Author(s) : qiu bin
// Email     : chat1@126.com
// Copyright (C) 2015 qiu bin 
// All rights reserved                
//-------------------------------------------------------------------------------------------------


module ram
(
clk,
wren, 
addr, 
data_in, 
data_out
);
parameter addr_bits = 3;
parameter data_bits = 224;
input     clk;
input     wren;
input     [addr_bits-1:0]  addr;
input     [data_bits-1:0]  data_in;
output    [data_bits-1:0]  data_out;

reg       [data_bits-1:0]  ram[0:(1 << addr_bits) -1] /* synthesis syn_ramstyle="block_ram" */;
reg       [data_bits-1:0]  data_out;

//read
always @ ( posedge clk )
begin
    data_out <= ram[addr];
end 

//write
always @ (posedge clk)
begin
    if (wren)
        ram[addr] <= data_in;
end

endmodule
