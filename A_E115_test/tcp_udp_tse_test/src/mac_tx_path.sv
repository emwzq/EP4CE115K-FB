//////////////////////////////////////////////////////////////////////
////                                                              ////
////  tx_path                                                     ////
////                                                              ////
////  Description                                                 ////
////      tx data path                                            ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////

import type_defs::*;

module mac_tx_path(
    input rst_n,
    ff_tx_if.s if_tx,
    headers_if if_headers_tx,
    input frame_type_t tx_type,
    input tx_start,
    input [13:0] tx_dword_count,
    output logic fifo_rdreq,
    input  fifo_empty,
    input [31:0] fifo_q,
    input [1:0] data_recv_mod
);


logic clk;
assign clk = if_tx.ff_tx_clk;
assign if_tx.ff_tx_crc_fwd = 1'b0;
assign if_tx.ff_tx_err = 1'b0;

typedef enum logic [3:0]{
    Idle = 4'b0,
    SOP,
    MAC,
    ARP,
    IP,
    ICMP,
    UDP,
    TCP,
    TCP_OPTIONS,
    Data,
    DataEOP,
    HeaderEOP
}Estate;

logic[3:0] state, next_state;

wire [0:6] [31:0] headers_buff;
assign headers_buff = if_headers_tx.data_out[223:0];

logic [2:0] prev_addr2;
always@(posedge clk or negedge rst_n)
if (~rst_n) begin
    prev_addr2 <= 0;
end
else if (if_tx.ff_tx_rdy) begin
    prev_addr2 <= if_headers_tx.addr2;
end

always@(*) begin
    if_headers_tx.addr2 = '0;
    case (next_state)
    SOP, MAC :if_headers_tx.addr2 = RAM_MAC_ADDR;
    ARP : if_headers_tx.addr2 = RAM_ARP_ADDR;
    IP : if_headers_tx.addr2 = RAM_IP_ADDR;
    UDP : if_headers_tx.addr2 = RAM_UDP_ADDR;
    TCP : if_headers_tx.addr2 = RAM_TCP_ADDR;
    TCP_OPTIONS : if_headers_tx.addr2 = RAM_TCP_OPTIONS_ADDR; 
    ICMP : if_headers_tx.addr2 = RAM_ICMP_ADDR;
    HeaderEOP : if_headers_tx.addr2 = prev_addr2;
    default : if_headers_tx.addr2 = '0;
    endcase
end

assign if_tx.ff_tx_data = (state == Data || state == DataEOP) ? fifo_q : headers_buff[header_counter];
assign fifo_rdreq = if_tx.ff_tx_rdy && 
        (next_state == Data || state == Data || next_state == DataEOP);
assign if_tx.ff_tx_mod = 2'b0;

logic [13:0] total_counter;
logic [3:0] header_counter;
always @(posedge clk  or negedge rst_n) begin
    if (!rst_n) begin
        if_tx.ff_tx_sop <= 0;
        if_tx.ff_tx_wren <= 0;
        if_tx.ff_tx_eop <= 0; 
        total_counter <= 0;
        header_counter <= 0;
    end
    else if (if_tx.ff_tx_rdy) begin
        if (next_state == DataEOP || next_state == HeaderEOP)begin
            if_tx.ff_tx_eop <= 1'b1;
            /*
            if (tx_dword_count >> 4)
                if_tx.ff_tx_mod <= data_recv_mod;
            else
                if_tx.ff_tx_mod <= 2'b0;
                */ 
        end
        case (state)
        Idle: begin
            if (next_state == SOP)begin
                header_counter <= 4'd0;
                total_counter <=  14'd0;
                if_tx.ff_tx_sop <= 1'b1;
                if_tx.ff_tx_wren <= 1'b1;
            end
        end
        SOP: begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
            if_tx.ff_tx_sop <= 1'b0;
        end
        MAC : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
            if (header_counter == 3) begin
                header_counter <= 0;
            end
        end
        ARP : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
        end
        IP : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
            if (next_state != IP) begin
                header_counter <= '0;
            end
        end
        UDP : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
        end
        TCP : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
            if (next_state == TCP_OPTIONS) begin
                header_counter <= 0;
            end
        end
        TCP_OPTIONS : begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
        end
        ICMP: begin
            header_counter <= header_counter + 1'b1;
            total_counter <=  total_counter + 1'b1;
        end
        Data: begin
            total_counter <=  total_counter + 1'b1;
        end
        HeaderEOP,DataEOP: begin
            if_tx.ff_tx_eop <= 1'b0;
            if_tx.ff_tx_wren <= 1'b0;
            header_counter <= 0;
            total_counter <= 0;
        end
        endcase
    end
end


always @(posedge clk or negedge rst_n)
if (~rst_n) begin
    state <= Idle;
end
else if (if_tx.ff_tx_rdy) begin
    state <= next_state;
end


always@(*)  begin
    next_state = state;
    case (state)
    Idle:begin
         if (tx_start) begin
            next_state = SOP;
         end
    end
    SOP: begin
        next_state = MAC;
    end
    MAC : begin
        if (header_counter == 3) begin
            if (tx_type == T_ARP_REQUEST || tx_type == T_ARP_REPLY) begin
                next_state = ARP;
            end
            else begin
                next_state = IP;
            end 
        end
    end
    ARP : begin
        if (header_counter == 5) begin
            next_state = HeaderEOP;
        end
    end
    IP : begin
        if (header_counter == 4) begin
            if (tx_type == T_ICMP_REPLY) begin
                next_state = ICMP;
            end
            else if (tx_type == T_UDP) begin
                next_state = UDP;
            end
            else if (tx_type == T_TCP) begin
                next_state = TCP;
            end
            else begin
                next_state = Idle;
            end
        end
    end
    UDP : begin
        if (header_counter == 1) begin
            next_state = Data;
        end
        if (total_counter == tx_dword_count - 2 && fifo_empty)
            next_state = HeaderEOP;
        else if (total_counter == tx_dword_count - 2)
            next_state = DataEOP;
    end
    TCP : begin
        if (header_counter == 4) begin
            if (if_headers_tx.is_option_exist)
                next_state = TCP_OPTIONS;
            else
                next_state = Data;
        end
        if (total_counter == tx_dword_count - 2 && fifo_empty)
            next_state = HeaderEOP;
        else if (total_counter == tx_dword_count - 2)
            next_state = DataEOP;
    end
    TCP_OPTIONS : begin
        if (header_counter == 1) begin
            next_state = Data;
        end
        if (total_counter == tx_dword_count - 2 && fifo_empty)
            next_state = HeaderEOP;
        else if (total_counter == tx_dword_count - 2)
            next_state = DataEOP;
    end
    ICMP: begin
        if (header_counter == 1) begin
            next_state = Data;
        end
        if (total_counter == tx_dword_count - 2 && fifo_empty)
            next_state = HeaderEOP;
        else if (total_counter == tx_dword_count - 2)
            next_state = DataEOP;
    end
    Data: begin
        if (total_counter == tx_dword_count - 2)begin
            next_state = DataEOP;
        end
    end
    HeaderEOP,DataEOP: begin
        next_state = Idle;    
    end
    default : next_state = Idle;
    endcase
end

endmodule
