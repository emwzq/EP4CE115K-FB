module rst_ctrl(
	input ext_rst,
	output reg rst,
	input clk	
);
	reg [7:0] cnt;	
	reg r_ext_rst,rr_ext_rst;
	
	always @(posedge clk) 
	begin
		r_ext_rst <= ext_rst;
		rr_ext_rst <= r_ext_rst;
		if(r_ext_rst & (~rr_ext_rst))	//posedge,清空计数器
			cnt <= 8'h0;
		if(cnt != 8'hff)		/*一路小跑到8'hff后停下来*/
			cnt <= cnt + {1'h1};
		rst <= (cnt == 8'hff);		
	end

endmodule
