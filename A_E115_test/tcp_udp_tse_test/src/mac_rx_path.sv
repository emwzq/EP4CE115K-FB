//////////////////////////////////////////////////////////////////////
////                                                              ////
////  rx_path                                                     ////
////                                                              ////
////  Description                                                 ////
////      rx data path                                            ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////

import type_defs::*;

module mac_rx_path(
    input rst_n,
    ff_rx_if.s if_rx,
    headers_if if_headers_rx, 
    output frame_type_t rx_type,
    output logic rx_done,

    output logic [31:0] data_recv,
    output logic data_recv_start,
    output logic data_recv_valid,
    output logic [15:0] data_recv_len,
    output logic [1:0] data_recv_mod,
    output u32_t cur_ripaddr,
    output u16_t cur_rport,

    input  logic rx_done_clear
);
logic clk;
assign clk = if_rx.ff_rx_clk;
assign if_rx.ff_rx_rdy = ~(rx_done || parse_state == RxDone);

logic [31:0] ff_rx_data;
logic ff_rx_eop;
assign ff_rx_data = if_rx.ff_rx_data;
assign ff_rx_eop = if_rx.ff_rx_eop;

arp_header_t arp_header;
ipv4_header_t ip_header;
udp_header_t udp_header;
tcp_header_t tcp_header;
icmp_headr_t icmp_header;

enum logic [3:0] {
    Idle = 4'b0,
    MACHeader,
    ARPHeader,
    IPv4Header,
    UDPHeader,
    TCPHeader,
    TCP_Options,
    ICMPHeader,
    Data,
    WaitForEOP,
    ParseTCP_Options,
    RxDone
} parse_state, next_parse_state;

enum logic[3:0] {
    TCP_OPTS_Idle,
    TCP_OPTS_Kind,
    TCP_OPTS_MSS1,
    TCP_OPTS_MSS2,
    TCP_OPTS_MSS3,
    TCP_OPTS_SCALE1,
    TCP_OPTS_SCALE2,
    TCP_OPTS_SACK_PERMITTED,
    TCP_OPTS_TS
}E2;

wire [27:0][7:0] tcp_opts_buff;

assign arp_header = if_headers_rx.data_out[223:0];
assign ip_header = if_headers_rx.data_out[159:0];
assign icmp_header = if_headers_rx.data_out[63:0];
assign udp_header = if_headers_rx.data_out[63:0];
assign tcp_header = if_headers_rx.data_out[159:0];
assign tcp_opts_buff = if_headers_rx.data_out[223:0];

assign if_headers_rx.data_in = {7{ff_rx_data}};

always@(*) begin
    case (parse_state)
    Idle,MACHeader : if_headers_rx.addr1 = RAM_MAC_ADDR;
    ARPHeader : if_headers_rx.addr1 = RAM_ARP_ADDR;
    IPv4Header : if_headers_rx.addr1 = RAM_IP_ADDR;
    UDPHeader : if_headers_rx.addr1 = RAM_UDP_ADDR;
    TCPHeader : if_headers_rx.addr1 = RAM_TCP_ADDR;
    TCP_Options: if_headers_rx.addr1 = RAM_TCP_OPTIONS_ADDR;
    ICMPHeader : if_headers_rx.addr1 = RAM_ICMP_ADDR;
    ParseTCP_Options: if_headers_rx.addr1 = RAM_TCP_OPTIONS_ADDR;
    default : if_headers_rx.addr1 = RAM_UNUSED_ADDR;
    endcase
end

logic [15:0] counter;
logic [3:0] counter10;
logic [15:0] ip_header_len;
logic [3:0] tcp_header_len;
logic [3:0] tcp_options_parse_state;
logic end_of_tcp_opts;

always @(*) begin
    next_parse_state <= parse_state;
    case ({if_rx.ff_rx_dval,parse_state})
    {1'b1,Idle}: begin
        if (if_rx.ff_rx_sop)begin
            next_parse_state <= MACHeader;
        end 
    end
    {1'b1,MACHeader}: begin
        if (counter == 1)begin
            if (ff_rx_data[15:0] == 16'h806) begin
                next_parse_state <= ARPHeader;
            end
            else if (ff_rx_data[15:0] == 16'h800)begin
                next_parse_state <= IPv4Header;
            end
            else begin
                next_parse_state <= WaitForEOP;
            end 
        end
    end
    {1'b1,ARPHeader} :begin
        if (counter == 1 && ff_rx_eop)begin
            next_parse_state <= RxDone;
        end
        else if(counter == 1)begin
            next_parse_state <= WaitForEOP;
        end
    end
    {1'b1,IPv4Header} : begin
        if (counter == 1 && ff_rx_data == IP_Addr) begin
            if (ip_header.proto == PROTO_ICMP) begin
                next_parse_state <= ICMPHeader;
            end
            else if (ip_header.proto == PROTO_UDP) begin
                next_parse_state <= UDPHeader;
            end
            else if (ip_header.proto == PROTO_TCP)begin
                next_parse_state <= TCPHeader;
            end
            else if (!ff_rx_eop) begin
                next_parse_state <= WaitForEOP;
            end
            else begin
                next_parse_state <= Idle;
            end
        end
        else if (counter == 1 && !ff_rx_eop) begin
            next_parse_state <= WaitForEOP;
        end
        else if (counter == 1) begin
            next_parse_state <= Idle;
        end
    end
    {1'b1,UDPHeader} : begin
        if (counter == 1 && udp_header.destport == UDP_Port) begin
            if ((ip_header_len - 20 - 8) > 0) 
                next_parse_state <= Data;
            else
                next_parse_state <= Idle;
        end
        else if (counter == 1 && !ff_rx_eop) begin
            next_parse_state <= WaitForEOP;
        end
        else if (counter == 1) begin
            next_parse_state <= Idle;
        end
    end
    {1'b1,TCPHeader} : begin
        if (counter == 1 && tcp_header.destport == TCP_Port && 
            tcp_header.header_len == 5 && (ip_header_len - 40) > 0 ) begin
            next_parse_state <= Data;
        end
        else if (counter == 1 && tcp_header.destport == TCP_Port && 
            tcp_header.header_len == 5) begin
            next_parse_state <= RxDone;
        end
        else if (counter == 1 && tcp_header.destport == TCP_Port &&
                tcp_header.header_len > 5) begin
            next_parse_state <= TCP_Options;
        end
        else if (counter == 1 && !ff_rx_eop) begin
            next_parse_state <= WaitForEOP;
        end
        else if (counter == 1) begin
            next_parse_state <= Idle;
        end
    end
    {1'b1,TCP_Options} : begin
        if (counter == 1 && (ip_header_len - 20 - tcp_header_len*4) > 0) begin
            next_parse_state <= Data;
        end
        else if (counter == 1) begin
            next_parse_state <= ParseTCP_Options;
        end
    end
    {1'b1,ICMPHeader} : begin
        if (counter == 1 && icmp_header.icmp_type == 8'd8 && 
            icmp_header.code == 8'd0) begin
            next_parse_state <= Data;
        end
        else if (counter == 1)begin
            next_parse_state <= WaitForEOP;
        end
    end
    {1'b1,Data} : begin
        if (counter <= 4 || ff_rx_eop) begin
            if (ff_rx_eop && if_headers_rx.is_option_exist) begin
                next_parse_state <= ParseTCP_Options;
            end
            else if (ff_rx_eop) begin
                next_parse_state <= RxDone;
            end
            else begin
                next_parse_state <= WaitForEOP;
            end
        end
    end
    {1'b1,WaitForEOP},{1'b0,WaitForEOP}: begin
        if (ff_rx_eop) begin
            if (if_headers_rx.is_option_exist)
                next_parse_state <= ParseTCP_Options;
            else
                next_parse_state <= RxDone;
        end
    end
    {1'b1,ParseTCP_Options},{1'b0,ParseTCP_Options}: begin
        if (counter == 0 || end_of_tcp_opts) begin
            next_parse_state <= RxDone;
        end
    end
    {1'b1,RxDone},{1'b0,RxDone}: begin
        next_parse_state <= Idle;
    end
    default : next_parse_state <= parse_state;
    endcase
end

always @(posedge clk or negedge rst_n)
if (~rst_n) begin
    parse_state <= Idle;
end
else begin
    parse_state <= next_parse_state;
end


always @(posedge clk or negedge rst_n)
if (!rst_n) begin:reset
    counter <= 10'b0;
    rx_type <= T_UNKNOWN;
    cur_ripaddr <= 0;
    cur_rport <= 0;
    data_recv_len <= 16'b0;
    ip_header_len <= 16'b0;
    tcp_header_len <= 4'b0;
    rx_done <= 1'b0;
    data_recv_mod <= 2'b0;
    end_of_tcp_opts <= 1'b0;
    tcp_options_parse_state <= TCP_OPTS_Idle;
    if_headers_rx.tcp_opt_mss <= '0;
    if_headers_rx.tcp_opt_scale <= '0;
    if_headers_rx.is_option_exist <= '0;
end
else begin
    if (rx_done_clear) begin
        rx_done <= 1'b0;
    end
    if (counter > 0 && if_rx.ff_rx_dval) begin
        counter <= counter - 1'b1;
    end 
    case({if_rx.ff_rx_dval,parse_state})
    {1'b1,Idle}:begin
        if (next_parse_state == MACHeader)begin
            counter <= 3;
            if_headers_rx.is_option_exist <= 1'b0;
            data_recv_len <= 0;
        end 
    end 
    {1'b1,MACHeader}: begin
        if (next_parse_state == ARPHeader) begin
            counter <= 7;
        end
        else if (next_parse_state == IPv4Header)begin
            counter <= 5;
        end
        else if (next_parse_state == WaitForEOP)begin
            rx_type <= T_UNKNOWN;
        end
    end
    {1'b1,ARPHeader}:begin
        if (next_parse_state == RxDone || next_parse_state == WaitForEOP)begin
            if (arp_header.operation_type == ARP_REQUEST) begin
                rx_type <= T_ARP_REQUEST;
            end
            else if (arp_header.operation_type == ARP_REPLY) begin
                rx_type <= T_ARP_REPLY;
            end
        end
    end
    {1'b1,IPv4Header}: begin
        if (next_parse_state == ICMPHeader) begin
            cur_ripaddr <= ip_header.src_ipaddr;
            counter <= 2;
            ip_header_len <= ip_header.len;
        end
        else if (next_parse_state == UDPHeader) begin
            cur_ripaddr <= ip_header.src_ipaddr;
            counter <= 2;
            ip_header_len <= ip_header.len;
        end
        else if (next_parse_state == TCPHeader) begin
            cur_ripaddr <= ip_header.src_ipaddr;
            counter <= 5;
            ip_header_len <= ip_header.len;
        end
        else if (next_parse_state == WaitForEOP)begin
            rx_type <= T_UNKNOWN;
        end
    end
    {1'b1,UDPHeader} : begin
        if (next_parse_state == Data) begin
            rx_type <= T_UDP;
            data_recv_len <= 16'(ip_header_len - 20 - 8);
            counter <= 16'(ip_header_len - 20 - 8);
        end
        else if (next_parse_state == WaitForEOP)begin
            rx_type <= T_UNKNOWN;
        end
    end
    {1'b1,TCPHeader} : begin
        rx_type <= T_TCP;
        if (next_parse_state == TCP_Options) begin
            counter <= 16'(tcp_header.header_len - 3'd5);
            if_headers_rx.is_option_exist <= 1'b1;
            tcp_header_len <= tcp_header.header_len;
            cur_rport <= tcp_header.srcport;
        end
        else if (next_parse_state == Data) begin
            data_recv_len <= 16'(ip_header_len - 20 - 20);
            counter <= 16'(ip_header_len - 20 - 20);
            cur_rport <= tcp_header.srcport;
        end
        else if (next_parse_state == RxDone) begin
            data_recv_len <= 0;
            cur_rport <= tcp_header.srcport;
        end
        else if (next_parse_state == WaitForEOP)begin
            rx_type <= T_UNKNOWN;
        end
    end
    {1'b1,TCP_Options} : begin
        if (next_parse_state == Data) begin
            data_recv_len <= 16'(ip_header_len - 20 - tcp_header_len*4);
            counter <= 16'(ip_header_len - 20 - tcp_header_len*4);
        end
        else if (next_parse_state == ParseTCP_Options) begin
            counter <= 16'(tcp_header_len*4 - 21);     
            tcp_options_parse_state <= TCP_OPTS_Idle;   
        end
    end
    {1'b1,ICMPHeader} : begin
        if (next_parse_state == Data) begin
            rx_type <= T_ICMP_REQUEST;
            data_recv_len <= 16'(ip_header_len - 20 - 8); 
            counter <= 16'(ip_header_len - 20 - 8); 
        end
        else if (next_parse_state == WaitForEOP)begin
            rx_type <= T_UNKNOWN;
        end
    end
    {1'b1,Data} : begin
        counter <= counter - 3'd4;
        if (next_parse_state == RxDone) begin
            data_recv_mod <= 2'(3'd4 - counter[2:0]);
        end
        else if (next_parse_state == ParseTCP_Options) begin
            counter <= 16'(tcp_header_len*4 - 21); 
            tcp_options_parse_state <= TCP_OPTS_Idle;   
        end
    end
    {1'b1,ParseTCP_Options},{1'b0,ParseTCP_Options} : begin
        counter <= counter - 1'b1;
        case (tcp_options_parse_state)
        TCP_OPTS_Idle : begin
            counter <= counter;
            end_of_tcp_opts <= 1'b0;
            tcp_options_parse_state <= TCP_OPTS_Kind;
        end
        TCP_OPTS_Kind : begin
            case(tcp_opts_buff[counter])
            8'd0 : end_of_tcp_opts <= 1;
            8'd1 : tcp_options_parse_state <= TCP_OPTS_Kind;
            8'd2 : tcp_options_parse_state <= TCP_OPTS_MSS1;
            8'd3 : tcp_options_parse_state <= TCP_OPTS_SCALE1;
            8'd4 : tcp_options_parse_state <= TCP_OPTS_SACK_PERMITTED;
            8'd8 : begin
                tcp_options_parse_state <= TCP_OPTS_TS;
                counter10 <= 9;
            end
            default : begin
                end_of_tcp_opts <= 1;
                assert(0);
            end
            endcase
        end
        TCP_OPTS_MSS1 : begin
            tcp_options_parse_state <= TCP_OPTS_MSS2;
            A2:assert(tcp_opts_buff[counter] == 4);
        end
        TCP_OPTS_MSS2 : begin
            tcp_options_parse_state <= TCP_OPTS_MSS3;
            if_headers_rx.tcp_opt_mss[15:8] <= tcp_opts_buff[counter];
        end
        TCP_OPTS_MSS3 : begin
            tcp_options_parse_state <= TCP_OPTS_Kind;
            if_headers_rx.tcp_opt_mss[7:0] <= tcp_opts_buff[counter];
        end
        TCP_OPTS_SCALE1 : begin
            tcp_options_parse_state <= TCP_OPTS_SCALE2;
            A3:assert(tcp_opts_buff[counter] == 3);
        end
        TCP_OPTS_SCALE2 : begin
            if_headers_rx.tcp_opt_scale <= tcp_opts_buff[counter];
            tcp_options_parse_state <= TCP_OPTS_Kind;
        end
        TCP_OPTS_SACK_PERMITTED : begin
            tcp_options_parse_state <= TCP_OPTS_Kind;
            A4:assert(tcp_opts_buff[counter] == 2);
        end
        TCP_OPTS_TS : begin
            counter10 <= counter10 - 1'b1;
            if (counter10 == 1)
                tcp_options_parse_state <= TCP_OPTS_Kind;
        end
        default : begin
            assert(0);
            end_of_tcp_opts <= 1;
        end
        endcase
    end
    {1'b1,RxDone},{1'b0,RxDone} : begin
        if (rx_type != T_UNKNOWN)
            rx_done <= 1'b1;
        counter <= 0;
    end
    endcase
end

always_comb 
if (if_headers_rx.wren) begin
    case (counter)
    7:if_headers_rx.be = 28'hf000000;
    6:if_headers_rx.be = 28'h0f00000;
    5:if_headers_rx.be = 28'h00f0000;
    4:if_headers_rx.be = 28'h000f000;
    3:if_headers_rx.be = 28'h0000f00;
    2:if_headers_rx.be = 28'h00000f0;
    1:if_headers_rx.be = 28'h000000f;
    0:if_headers_rx.be = 28'h000f000;
    default:if_headers_rx.be = 28'hfffffff;
    endcase
end
else begin
    if_headers_rx.be = 28'hfffffff;
end

assign if_headers_rx.sel = (parse_state != Idle || if_rx.ff_rx_sop) && parse_state != RxDone;    
assign if_headers_rx.wren = if_rx.ff_rx_dval && parse_state != Data && 
                         parse_state != ParseTCP_Options &&
                        (parse_state != Idle || if_rx.ff_rx_sop);

assign data_recv_start = parse_state != Data && next_parse_state == Data;
assign data_recv_valid = (parse_state == Data && if_rx.ff_rx_dval);
assign data_recv = ff_rx_data;

endmodule
