//////////////////////////////////////////////////////////////////////
////                                                              ////
////  tcpip_hw                                                    ////
////                                                              ////
////  Description                                                 ////
////      top module                                              ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////

module tcpip_hw(	
	input clk,
	input rst_n,
	
	output mdc,
	inout  mdio,
	output phy_rst_n,
	
	output is_link_up,

	input rx_clk,
	input [7:0] rx_data,
	input rx_data_valid,
	output gtx_clk,
	input  tx_clk,	
	output logic [7:0] tx_data,
	output logic tx_en
);
import type_defs::*;
   
wire mdio_oen;
wire mdio_out;
wire mdio_in;

assign mdio = mdio_oen == 1'b0 ? mdio_out : 1'bz; 
assign mdio_in = mdio;

wire reset_n;
wire clk25;
wire clk50;
wire clk125;
wire clk250;
assign gtx_clk = clk125;

pll pll_inst(
    .inclk0(clk),
    .c0(clk50),
	.c1(clk125),
	.c2(clk250)
);

rst_ctrl rst_ctrl_inst(
	.ext_rst(rst_n),
	.rst(reset_n),
	.clk(clk50)	
);


assign phy_rst_n = reset_n;

wire tse_tx_clk;
wire eth_mode;

assign tse_tx_clk = eth_mode ? clk125 : tx_clk;

wire [7:0] gm_tx_d;
wire gm_tx_en;
wire [3:0] m_tx_d;
wire m_tx_en;

always @(posedge clk125 or negedge reset_n) 
if (~reset_n) begin
	tx_data <= 0;
	tx_en <= 0;
end
else begin
	tx_data <= eth_mode ? gm_tx_d : {4'b0, m_tx_d[3:0]};
	tx_en <= eth_mode ? gm_tx_en : m_tx_en;
end


headers_if if_headers_rx(clk50);
headers_if if_headers_tx(clk50);
ff_tx_if if_tx(clk50);
ff_rx_if if_rx(clk50);

frame_type_t rx_type;
frame_type_t tx_type;

logic rx_done;
logic [31:0] data_recv;
logic [15:0] data_recv_len;
logic [1:0] data_recv_mod;
logic data_recv_valid;
logic data_recv_start;
logic rx_done_clear;
u32_t cur_ripaddr;
u16_t cur_rport;

logic tx_start;
logic [13:0] tx_dword_count;
logic fifo_rdreq;
logic [31:0] fifo_q;
logic fifo_empty;
logic pkt_send_eop;


assign pkt_send_eop = if_tx.ff_tx_eop;
assign data_send_len = 0;

mac_rx_path mac_rx_path_inst(
    .rst_n(reset_n),
    .*
);

mac_tx_path  mac_tx_path_inst (
    .rst_n(reset_n),
    .*
);

eth_fsm eth_fsm_inst(
	.clk(clk50),
	.rst_n(reset_n),
   .*
);

wire [7:0] tse_addr;
wire [31:0] tse_rd_data;
wire [31:0] tse_wr_data;
wire tse_rd;
wire tse_wr;
wire tse_busy;
wire is_10m;
wire is_100m;
wire is_1000m;

mac_config mac_config_inst (
	.clk(clk50),
	.rst_n(reset_n),
	.*
);


tse  tse_inst(
	.clk(clk50),           // control_port_clock_connection.clk
	.reset(~reset_n),         //              reset_connection.reset
	.address(tse_addr),       //                  control_port.address
	.readdata(tse_rd_data),      //                              .readdata
	.read(tse_rd),          //                              .read
	.writedata(tse_wr_data),     //                              .writedata
	.write(tse_wr),         //                              .write
	.waitrequest(tse_busy),   //                              .waitrequest
	
	.tx_clk(tse_tx_clk),        //   pcs_mac_tx_clock_connection.clk
	.rx_clk(rx_clk),        //   pcs_mac_rx_clock_connection.clk
	
    .set_10(is_10m&&!is_100m),        //         mac_status_connection.set_10
	.set_1000(is_1000m),      //                              .set_1000
	.eth_mode(eth_mode),      //                              .eth_mode
	.ena_10(),        //                              .ena_10
	
	.gm_rx_d(rx_data),       //           mac_gmii_connection.gmii_rx_d
	.gm_rx_dv(rx_data_valid),      //                              .gmii_rx_dv
	.gm_rx_err(1'b0),     //                              .gmii_rx_err
	
	.gm_tx_d(gm_tx_d),       //                              .gmii_tx_d
	.gm_tx_en(gm_tx_en),      //                              .gmii_tx_en
	.gm_tx_err(),     //                              .gmii_tx_err
	
	.m_rx_d(rx_data[3:0]),        //            mac_mii_connection.mii_rx_d
	.m_rx_en(rx_data_valid),       //                              .mii_rx_dv
	.m_rx_err(1'b0),      //                              .mii_rx_err
	
    .m_tx_d(m_tx_d[3:0]),        //                              .mii_tx_d
	.m_tx_en(m_tx_en),       //                              .mii_tx_en
	.m_tx_err(),      //                              .mii_tx_err
	.m_rx_crs(1'b0),      //                              .mii_crs
	.m_rx_col(1'b0),      //                              .mii_col
	
	.ff_rx_clk(if_rx.ff_rx_clk),     //      receive_clock_connection.clk
	.ff_rx_data(if_rx.ff_rx_data),    //                       receive.data
	.ff_rx_eop(if_rx.ff_rx_eop),     //                              .endofpacket
	.rx_err(if_rx.rx_err),        //                              .error
	.ff_rx_mod(if_rx.ff_rx_mod),     //                              .empty
	.ff_rx_rdy(if_rx.ff_rx_rdy),     //                              .ready
	.ff_rx_sop(if_rx.ff_rx_sop),     //                              .startofpacket
	.ff_rx_dval(if_rx.ff_rx_dval),    //                              .valid
    .rx_err_stat(if_rx.rx_err_stat),   //                              .rx_err_stat
    .rx_frm_type(if_rx.rx_frm_type),   //                              .rx_frm_type
    .ff_rx_dsav(if_rx.ff_rx_dsav),    //                              .ff_rx_dsav
    .ff_rx_a_full(if_rx.ff_rx_a_full),  //                              .ff_rx_a_full
    .ff_rx_a_empty(if_rx.ff_rx_a_empty),  //       

    
    .ff_tx_clk(if_tx.ff_tx_clk),
    .ff_tx_data(if_tx.ff_tx_data),    
    .ff_tx_eop(if_tx.ff_tx_eop),     
    .ff_tx_err(if_tx.ff_tx_err),     
    .ff_tx_mod(if_tx.ff_tx_mod),     
    .ff_tx_rdy(if_tx.ff_tx_rdy),     
    .ff_tx_sop(if_tx.ff_tx_sop),     
    .ff_tx_wren(if_tx.ff_tx_wren),    
    .ff_tx_septy(if_tx.ff_tx_septy),   
    .tx_ff_uflow(if_tx.tx_ff_uflow),    
    .ff_tx_a_empty(if_tx.ff_tx_a_empty),  
    .ff_tx_crc_fwd(if_tx.ff_tx_crc_fwd),
    .ff_tx_a_full(if_tx.ff_tx_a_full),

	.xon_gen(1'b0),       //           mac_misc_connection.xon_gen
	.xoff_gen(1'b0),
		
    .mdc(mdc),           //           mac_mdio_connection.mdc
	.mdio_in(mdio_in),       //                              .mdio_in
	.mdio_out(mdio_out),      //                              .mdio_out
	.mdio_oen(mdio_oen)      //                              .mdio_oen
);


endmodule