//////////////////////////////////////////////////////////////////////
////                                                              ////
////  type_defs package                                           ////
////                                                              ////
////  Description                                                 ////
////      define data types and global parameters                 ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////
`define RGMII_IF

`ifndef INCLUDED_types_def
`define INCLUDED_types_def
package type_defs;
typedef logic [3:0]  u4_t;
typedef logic [7:0]  u8_t;
typedef logic [15:0] u16_t;
typedef logic [31:0] u32_t;
typedef logic [63:0] u64_t;

parameter RAM_MAC_ADDR = 3'd0,
RAM_ARP_ADDR = 3'd1,
RAM_IP_ADDR = 3'd2,
RAM_UDP_ADDR = 3'd3,
RAM_TCP_ADDR = 3'd4,
RAM_TCP_OPTIONS_ADDR = 3'd5,
RAM_ICMP_ADDR = 3'd6,
RAM_UNUSED_ADDR = 3'd7;

parameter
ARP_REQUEST = 16'h1,
ARP_REPLY = 16'h2;

typedef enum logic [2:0] {
    T_ARP_REQUEST = 3'b1,
    T_ARP_REPLY,
    T_ICMP_REQUEST,
    T_ICMP_REPLY,
    T_UDP,
    T_TCP,
    T_UNKNOWN
} frame_type_t;

typedef struct packed {
u16_t padding;
logic [47:0] dst_addr;
logic [47:0] src_addr;
u16_t mac_type;
} mac_header_t;

typedef struct packed {
    u16_t hardware_type;
    u16_t protocol_type;
    u8_t hardware_addr_len;
    u8_t protocol_addr_len;
    u16_t operation_type;
    logic [47:0] src_addr;
    u32_t src_ip_addr;
    logic [47:0] dst_addr;
    u32_t dst_ip_addr;
} arp_header_t;

parameter PROTO_ICMP = 8'd1,
PROTO_TCP = 8'd6,
PROTO_UDP = 8'd17;

typedef struct packed{
    u4_t version;
    u4_t header_len;
    u8_t tos;
    u16_t len;
    u16_t ipid;
    logic reserved;
    logic b_no_fragment;
    logic [5:0] reserved1;
    u8_t ipoffset;
    u8_t ttl;
    u8_t proto;
    u16_t checksum;
    u32_t src_ipaddr;
    u32_t dst_ipaddr;
} ipv4_header_t;

typedef struct packed{ 
    u8_t icmp_type;
    u8_t code;
    u16_t checksum;
    u16_t id;
    u16_t seqno;
}icmp_headr_t;  


typedef struct packed{
u32_t src_addr;  
u32_t dst_addr;     
u8_t mbz; 
u8_t protocal;   
u16_t len;
}psdheader_t;

typedef struct packed{
    u16_t srcport;
    u16_t destport;
    u16_t len;
    u16_t checksum;
}udp_header_t;


typedef enum logic[2:0] {
    TCP_LISTEN = 3'd0,
    TCP_SYN_RCVD,
    TCP_ESTABLISHED,
    TCP_CLOSE_WAIT,
    TCP_LAST_ACK
}E_TCP_STATE;

typedef struct packed {
    u16_t srcport;
    u16_t destport;
    u32_t seqno;
    u32_t ackno;
    u4_t header_len;
    logic [3:0] reserved;
    struct packed {
        logic ECE;
        logic CWR;
        logic URG;
        logic ACK;
        logic PSH;
        logic RST;
        logic SYN;
        logic FIN;
    }flags;
    u16_t window_size;
    u16_t checksum;
    u16_t urgp;
}tcp_header_t;

typedef struct packed{ 
    u8_t kind_mss;
    u8_t mass_len;
    u16_t mss;
    u8_t kind_window_scale;
    u8_t window_scale_len;
    u8_t window_scale;
    u8_t end_of_opt;
}tcp_opts_header_t; 

typedef struct packed{
  u32_t ripaddr;      /**< The IP address of the remote host. */
  u16_t lport;        /**< The local TCP port. */
  u16_t rport;        /**< The local remote TCP port.*/
  u32_t rcv_nxt;      /**< The sequence number that we expect to receive next. */
  u32_t snd_nxt;      /**< The sequence number that was last sent by us. */
  u16_t len;          /**< Length of the data that was previously sent. */
  u16_t mss;          /**< Current maximum segment size for the connection. */
  E_TCP_STATE tcp_state;      /**< TCP state. */
  u8_t timer;         /**< The retransmission timer. */
  u8_t num_retrans;   /**< The number of retransmissions for the last segment sent. */
} tcp_conn_t;

parameter TCP_RTO = 2'd3;

parameter tcp_opts_header_checksum = 16'hd07;

parameter MAC_Addr = 48'h001C23174ACC;
parameter IP_Addr = {8'd192, 8'd168, 8'd2, 8'd103};
parameter UDP_Port = 16'd6677;
parameter TCP_Port = 16'd6688;
parameter TargetIP_Addr = {8'd192, 8'd168, 8'd1, 8'd102};

typedef struct packed{
  u32_t ipaddr;
  logic[47:0] ethaddr;
} arp_entry_t;

endpackage


`endif
