//////////////////////////////////////////////////////////////////////
////                                                              ////
////  tcp_ip_hw_ifs                                               ////
////                                                              ////
////  Description                                                 ////
////      define interfaces                                       ////
////                                                              ////
////  Author(s):                                                  ////
////      - bin qiu, qiubin@opencores.org or  chat1@126.com       ////
////                                                              ////
////                   Copyright (C) 2015                         ////
//////////////////////////////////////////////////////////////////////

interface ff_rx_if(input ff_rx_clk);
logic [31:0] ff_rx_data;
logic ff_rx_eop;
logic ff_rx_sop;
logic [5:0] rx_err;
logic [1:0] ff_rx_mod;
logic ff_rx_dval;
logic ff_rx_rdy;
logic [17:0] rx_err_stat; 
logic [3:0]  rx_frm_type; 
logic        ff_rx_dsav;  
logic        ff_rx_a_full;
logic        ff_rx_a_empty; 

modport m(
        input  ff_rx_clk,
        output ff_rx_data,
        output ff_rx_eop,
        output ff_rx_sop,
        output rx_err,
        output ff_rx_mod,
        output ff_rx_dval,
        output rx_err_stat,  
        output rx_frm_type,  
        output ff_rx_dsav,   
        output ff_rx_a_full, 
        output ff_rx_a_empty,
        input  ff_rx_rdy
        );
        
modport s(
        input ff_rx_clk,
        input ff_rx_data,
        input ff_rx_eop,
        input ff_rx_sop,
        input rx_err,
        input ff_rx_mod,
        input ff_rx_dval,
        input  rx_err_stat,  
        input  rx_frm_type,  
        input  ff_rx_dsav,   
        input  ff_rx_a_full, 
        input  ff_rx_a_empty,
        output ff_rx_rdy
        );

endinterface

interface ff_tx_if(input ff_tx_clk);
logic [31:0] ff_tx_data;   
logic        ff_tx_eop;     
logic        ff_tx_err;     
logic [1:0]  ff_tx_mod;     
logic        ff_tx_rdy;     
logic        ff_tx_sop;     
logic        ff_tx_wren;   
logic        ff_tx_crc_fwd; 
logic        ff_tx_septy; 
logic        tx_ff_uflow;   
logic        ff_tx_a_full;  
logic        ff_tx_a_empty; 


modport m(
        input  ff_tx_clk,
        input  ff_tx_data,    
        input  ff_tx_eop,     
        input  ff_tx_err,     
        input  ff_tx_mod,     
        output ff_tx_rdy,     
        input  ff_tx_sop,     
        input  ff_tx_wren,    
        input  ff_tx_crc_fwd, 
        output ff_tx_septy,   
        output tx_ff_uflow,   
        output ff_tx_a_full,  
        output ff_tx_a_empty
);

modport s(               
        input  ff_tx_clk,    
        output ff_tx_data,   
        output  ff_tx_eop,    
        output  ff_tx_err,    
        output  ff_tx_mod,    
        input  ff_tx_rdy, 
        output  ff_tx_sop,    
        output  ff_tx_wren,   
        output  ff_tx_crc_fwd,
        input  ff_tx_septy,  
        input  tx_ff_uflow,  
        input  ff_tx_a_full, 
        input ff_tx_a_empty 
); 
endinterface

interface headers_if(input clk);
import type_defs::*;         
parameter addr_bits = 3;
parameter data_bits = 8*28;//224 ARP header size
logic     wren;
logic     sel;
logic     [addr_bits-1:0]   addr;
logic     [addr_bits-1:0]   addr1;
logic     [addr_bits-1:0]   addr2;
logic     [data_bits-1:0]   data_in;
logic     [data_bits-1:0]   data_out;
logic     [data_bits/8-1:0] be;
assign addr = sel ? addr1 : addr2;
headers_ram headers_ram_inst(
    .address(addr),
    .byteena(be),
    .clock(clk),
    .data(data_in),
    .wren(wren),
    .q(data_out));


logic [15:0] tcp_opt_mss;
logic [7:0] tcp_opt_scale;
logic is_option_exist;
endinterface
