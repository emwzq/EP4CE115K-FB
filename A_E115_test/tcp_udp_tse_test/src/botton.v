module botton
(
 clk,
 rst_n,
 key1,
 pulse
);
input clk;
input rst_n;
input key1;
output pulse;

reg key1_internal;
reg key1_internal_s;
reg [17:0] high;
reg [17:0] low;
reg pulse;

always @ (posedge clk or negedge rst_n)
if (!rst_n) begin
	high <= 0;
	low  <= 0;
	key1_internal <= 0;
end
else if (key1) begin
	if (high == 17'h1ffff) begin
		key1_internal <= 1'b1;
		high <= 0;
		low <= 0;
	end
	else begin
		high <= high + 1'b1;
		low <= 0;
	end
end
else begin
	if (low == 17'h1ffff) begin
		key1_internal <= 0;
		high <= 0;
		low <= 0;
	end
	else begin
		low <= low + 1;
		high <= 0;
	end
end

always @(posedge clk)
	key1_internal_s <= key1_internal;
	
always @(posedge clk or negedge rst_n)
if (!rst_n) begin
	pulse <= 0;
end
else if(!key1_internal && key1_internal_s)
	pulse <= 1;
else
	pulse <= 0;
	
endmodule
